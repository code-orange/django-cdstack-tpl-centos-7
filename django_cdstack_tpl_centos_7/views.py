from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
    generate_motd,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
)
from django_cdstack_tpl_icinga2_client.django_cdstack_tpl_icinga2_client.views import (
    handle as handle_icinga2_client,
)


def lookup_vars(template_opts, net_iface, var_in, var_out):
    if var_in in template_opts:
        net_iface[var_out] = template_opts[var_in]
    return


def get_yum_packages(image_yaml_path: str):
    return get_apt_packages(image_yaml_path)


def get_yum_sources(image_yaml_path: str):
    return get_apt_sources(image_yaml_path)


def handle(zipfile_handler, template_opts, cmdb_host, skip_network=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_centos_7/django_cdstack_tpl_centos_7"

    if "yum_packages" not in template_opts:
        template_opts["yum_packages"] = get_yum_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "yum_sources" not in template_opts:
        template_opts["yum_sources"] = get_yum_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if not skip_network:
        for key in template_opts.keys():
            if key.startswith("network_iface_eth") and key.endswith("_hwaddr"):
                key_ident = key[:-7]

                network_iface_split = key.split("_")
                network_iface = network_iface_split[2]

                net_iface = dict()
                net_iface["network_iface"] = network_iface

                lookup_vars(
                    template_opts,
                    net_iface,
                    key_ident + "_hwaddr",
                    "network_iface_hwaddr",
                )

                if key_ident + "_ip" in template_opts:
                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/sysconfig/network-scripts/ifcfg-eth",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    lookup_vars(
                        template_opts, net_iface, key_ident + "_ip", "network_iface_ip"
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_netmask",
                        "network_iface_netmask",
                    )
                    lookup_vars(
                        template_opts, net_iface, key_ident + "_gw", "network_iface_gw"
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_ip6",
                        "network_iface_ip6",
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_netmask6",
                        "network_iface_netmask6",
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_gw6",
                        "network_iface_gw6",
                    )

                    merged_opts = {**template_opts, **net_iface}

                    zip_add_file(
                        zipfile_handler,
                        "etc/sysconfig/network-scripts/ifcfg-" + network_iface,
                        config_template.render(merged_opts),
                    )

            if key.startswith("network_iface_bond") and key.endswith("_hwaddr"):
                key_ident = key[:-7]

                network_iface_split = key.split("_")
                network_iface = network_iface_split[2]

                net_iface = dict()
                net_iface["network_iface"] = network_iface

                lookup_vars(
                    template_opts,
                    net_iface,
                    key_ident + "_hwaddr",
                    "network_iface_hwaddr",
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/sysconfig/network-scripts/ifcfg-bond",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                merged_opts = {**template_opts, **net_iface}

                zip_add_file(
                    zipfile_handler,
                    "etc/sysconfig/network-scripts/ifcfg-" + network_iface,
                    config_template.render(merged_opts),
                )

            if key.startswith("network_iface_br") and key.endswith("_hwaddr"):
                key_ident = key[:-7]

                network_iface_split = key.split("_")
                network_iface = network_iface_split[2]

                net_iface = dict()
                net_iface["network_iface"] = network_iface

                lookup_vars(
                    template_opts,
                    net_iface,
                    key_ident + "_hwaddr",
                    "network_iface_hwaddr",
                )

                if key_ident + "_ip" in template_opts:
                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/sysconfig/network-scripts/ifcfg-br",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    lookup_vars(
                        template_opts, net_iface, key_ident + "_ip", "network_iface_ip"
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_netmask",
                        "network_iface_netmask",
                    )
                    lookup_vars(
                        template_opts, net_iface, key_ident + "_gw", "network_iface_gw"
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_ip6",
                        "network_iface_ip6",
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_netmask6",
                        "network_iface_netmask6",
                    )
                    lookup_vars(
                        template_opts,
                        net_iface,
                        key_ident + "_gw6",
                        "network_iface_gw6",
                    )

                    merged_opts = {**template_opts, **net_iface}

                    zip_add_file(
                        zipfile_handler,
                        "etc/sysconfig/network-scripts/ifcfg-" + network_iface,
                        config_template.render(merged_opts),
                    )

            if key.startswith("network_iface_ovsbr") and key.endswith("_name"):
                key_ident = key[:-5]

                network_iface_split = key.split("_")
                network_iface = network_iface_split[2]

                net_iface = dict()
                net_iface["network_iface"] = network_iface

                lookup_vars(
                    template_opts,
                    net_iface,
                    key_ident + "_hwaddr",
                    "network_iface_hwaddr",
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/sysconfig/network-scripts/ifcfg-ovsbr",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                merged_opts = {**template_opts, **net_iface}

                zip_add_file(
                    zipfile_handler,
                    "etc/sysconfig/network-scripts/ifcfg-" + network_iface,
                    config_template.render(merged_opts),
                )

            if key.startswith("network_iface_vlan_"):
                key_ident = key

                network_iface_split = key.split("_")
                network_iface = network_iface_split[3]

                net_iface = dict()
                net_iface["network_iface"] = network_iface
                net_iface["network_iface_vlanid"] = template_opts[key]

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/sysconfig/network-scripts/ifcfg-vlan",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                merged_opts = {**template_opts, **net_iface}

                zip_add_file(
                    zipfile_handler,
                    "etc/sysconfig/network-scripts/ifcfg-"
                    + network_iface
                    + "."
                    + net_iface["network_iface_vlanid"],
                    config_template.render(merged_opts),
                )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_icinga2_client(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if "yum_sources" in template_opts:
        for repo_name, yum_source in template_opts["yum_sources"].items():
            repo_section_counter = 0
            repo_file = str()

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/yum.repos.d/repository.repo",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            for repo_section in yum_source:
                repo_content = dict()
                repo_content["repo_name"] = repo_name + "-" + str(repo_section_counter)
                repo_content["repo_username"] = cmdb_host.id
                repo_content["repo_password"] = cmdb_host.api_key.key
                repo_content["yum_source"] = repo_section
                repo_file += config_template.render(repo_content) + "\n\n"

                repo_section_counter += 1

            zip_add_file(
                zipfile_handler, "etc/yum.repos.d/" + repo_name + ".repo", repo_file
            )

    # MOTD
    zip_add_file(
        zipfile_handler,
        "etc/motd",
        generate_motd(cmdb_host, template_opts),
    )

    return True
