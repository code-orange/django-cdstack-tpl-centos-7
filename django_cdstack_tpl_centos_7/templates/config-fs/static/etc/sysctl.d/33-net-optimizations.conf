net.ipv4.tcp_slow_start_after_idle=0 
net.ipv4.tcp_no_metrics_save=1
net.ipv4.ip_local_port_range=1024 65000
net.ipv4.tcp_timestamps=1
net.ipv4.tcp_window_scaling=1

# Increase number of incoming connections that can queue up
# before dropping
net.core.somaxconn=50000

# Handle SYN floods and large numbers of valid HTTPS connections
# https://shehab.tech/
net.ipv4.tcp_max_syn_backlog=30000

# tcp_max_tw_buckets is the maximum number of sockets in 'TIME_WAIT' state.
# After reaching this number the system will start destroying the socket that are in this state.
net.ipv4.tcp_max_tw_buckets=2000000

# tcp_tw_reuse sets whether TCP should reuse an existing connection in the TIME-WAIT state
# for a new outgoing connection if the new timestamp is strictly bigger than the most recent
# timestamp recorded for the previous connection.
net.ipv4.tcp_tw_reuse=1

# Increase the length of the network device input queue
net.core.netdev_max_backlog=100000
net.core.netdev_budget=50000
net.core.netdev_budget_usecs=5000

# If your servers talk UDP, also up these limits
net.ipv4.udp_rmem_min=8192
net.ipv4.udp_wmem_min=8192

# Disconnect dead TCP connections after 120 secs
net.ipv4.tcp_keepalive_time=60
net.ipv4.tcp_keepalive_intvl=10
net.ipv4.tcp_keepalive_probes=6

# Enable MTU probing
# The longer the MTU the better for performance, but the worse for reliability.
# This is because a lost packet means more data to be retransmitted and because
# many routers on the Internet can't deliver very long packets
net.ipv4.tcp_mtu_probing=1

# Protect against tcp time-wait assassination hazards, drop RST packets
# for sockets in the time-wait state.
# Not widely supported outside of Linux, but conforms to RFC.
net.ipv4.tcp_rfc1337=1
