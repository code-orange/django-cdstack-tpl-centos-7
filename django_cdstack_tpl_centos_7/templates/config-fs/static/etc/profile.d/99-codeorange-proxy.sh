#!/bin/bash

{% if http_proxy_server %}
# Set proxy
http_proxy={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}
https_proxy={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}
ftp_proxy={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}
HTTP_PROXY={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}
HTTPS_PROXY={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}
FTP_PROXY={{ http_proxy_proto }}://{{ http_proxy_server }}:{{ http_proxy_port }}

# Export proxy
export http_proxy
export https_proxy
export ftp_proxy
export HTTP_PROXY
export HTTPS_PROXY
export FTP_PROXY
{% endif %}
